#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

__title__ ="W9replay";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="0.10"
__usage__ ='''
M6 grabber for mythnetvision 0.25 and 0.26

> %(file)s -h
Usage: %(file)s [option][<search>]
Version: %(version)s Author: %(author)s

For details on the MythTV Netvision plugin see the wiki page at:
http://www.mythtv.org/wiki/MythNetvision

Options:
  -h, --help            show this help message and exit
  -v, --version         Display grabber name and supported options
  -S:, --search=        Search <search> for videos
  -T, --treeview        Display a Tree View of a sites videos
'''%{'file':__file__, 'version':__version__,'author':__author__}


import sys, os, locale, re

import simplejson as json

from time import time
from getopt import getopt
from xml.dom import minidom
from urllib2 import urlopen
from ConfigParser import RawConfigParser

import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s')
logger = logging.getLogger(__title__)

reload(sys)
sys.setdefaultencoding('utf-8')

# INIT
# Url de base
urlSite = 'http://www.w9replay.fr/'
urlApi = 'http://static.m6replay.fr/catalog/m6group_web/w9replay/'
urlImage   = 'http://static.m6replay.fr/images/'
urlPage   = 'http://www.w9replay.fr/#'
configFile = '/etc/mythtv/mythnetvision-w9replay.cfg'
userConfigFile = os.environ['HOME']+'/.mythtv/mythnetvision-w9replay.cfg'

# Lecteur externe devant être utilisé ('' pour le lecteur interne)
player = False
# Argument du lecteur (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
playerargs = False
# "Téléchargeur" ('' pour le téléchargeur par défaut)
download = False
# Argument du "téléchargeur" (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
downloadargs = False
# Description de channel
channel = {
        'title' : __title__,
        'link' : urlSite,
        'description' : 'Voir ou revoir les programmes de france télévisions'
    }


def MinidomNode_getFirstChildByTagName(self,tag) :
    '''
        Retourne le 1er enfant directe qui a pour tag "tag". Si aucun
        retourne None"
    '''
    for node in self.childNodes :
        if node.nodeType == node.ELEMENT_NODE and node.tagName == tag :
            return node
    return None
setattr(minidom.Node,'getFirstChildByTagName',MinidomNode_getFirstChildByTagName)


def MinidomNode_getChildByTagName(self,tag) :
    '''
        Retourne l'ensemble des enfants directes qui ont pour tag "tag".
        Si aucun une liste vide"
    '''
    nodes = []
    for node in self.childNodes :
        if node.nodeType == node.ELEMENT_NODE and node.tagName == tag :
            nodes.append(node)
    return nodes
setattr(minidom.Node,'getChildsByTagName',MinidomNode_getChildByTagName)


def MinidomNode_getChildByAttribute(self,attrname, attrvalue) :
    '''
        Retourne le 1er enfant directe qui a pour l'attribut "attrname" la
        valeur "attrvalue". Si aucun retourne False.
    '''
    for node in self.childNodes :
        if node.hasAttribute(attrname) :
            if node.getAttribute(attrname) == attrvalue :
                return node
    return False
setattr(minidom.Node,'getChildByAttribute',MinidomNode_getChildByAttribute)


def MinidomDocument_createNode(self,tag,text='',attributes={}) :
    '''
        Retourne un noeud avec pour tag "tag", comme text "text" ou comme
        attributs le dico "attributes" ({attrName: attrValue})
    '''
    node = self.createElement(tag)
    if text :
        node.appendChild(self.createTextNode(text))
    for (name,value) in attributes.items() :
        node.setAttribute(name,value)
    return node
setattr(minidom.Document,'createNode',MinidomDocument_createNode)


def domVersion() :
    domOutput = minidom.Document()
    rootNode = domOutput.appendChild(domOutput.createNode('grabber'))
    rootNode.appendChild(domOutput.createNode('name',__title__))
    rootNode.appendChild(domOutput.createNode('command',__file__))
    rootNode.appendChild(domOutput.createNode('author',__author__))
    rootNode.appendChild(domOutput.createNode('thumbnail','w9replay.png'))
    rootNode.appendChild(domOutput.createNode('type','video'))
    rootNode.appendChild(domOutput.createNode('description',channel['description']))
    rootNode.appendChild(domOutput.createNode('version',__version__))
    rootNode.appendChild(domOutput.createNode('search','true'))
    rootNode.appendChild(domOutput.createNode('tree','true'))
    return rootNode


def domItemNode(domOutput,clpValues,in_search=False) :
    itemOutputNode = domOutput.createNode('item')
    if in_search :
        itemOutputNode.appendChild(domOutput.createNode('title',clpValues['programName']+' - '+clpValues['clpName']))
    else :
        itemOutputNode.appendChild(domOutput.createNode('title',clpValues['clpName']))

    itemOutputNode.appendChild(domOutput.createNode('author',clpValues['copy']))
    itemOutputNode.appendChild(domOutput.createNode('pubDate',str(clpValues['publiDate'])+' CEST'))
    itemOutputNode.appendChild(domOutput.createNode('description',clpValues['desc']))
    itemOutputNode.appendChild(domOutput.createNode('link',urlPage+clpValues['code']))
    mediaOutputNode = itemOutputNode.appendChild(domOutput.createNode('media:group'))
    if type(clpValues['img']) == dict :
        thumbnail = urlImage+clpValues['img']['vignette']
    else :
        thumbnail = 'w9replay.png'
    mediaOutputNode.appendChild(domOutput.createNode(
            tag='media:thumbnail',
            attributes={
                    'url':thumbnail
        }))
    mediaOutputNode.appendChild(domOutput.createNode(
            tag='media:content',
            attributes={
                    #'url':baseUrlVideo+mediaInputNode.getAttribute('video_url'),#Mythnetvision ne support pas les flux rtmp
                    'url':urlPage+clpValues['code'],
                    'duration':'0'
        }))
    itemOutputNode.appendChild(domOutput.createNode('ratting','0'))
    if player :
        itemOutputNode.appendChild(domOutput.createNode('player',player))
    if playerargs :
        itemOutputNode.appendChild(domOutput.createNode('playerargs',playerargs))
    if download :
        itemOutputNode.appendChild(domOutput.createNode('download',download))
    if downloadargs :
        itemOutputNode.appendChild(domOutput.createNode('downloadargs',downloadargs))
    #~ if inputChild.hasAttribute('saison') :
    #~ itemOutputNode.appendChild(domOutput.createNode('saison',inputChild.getAttribute('saison')))
    #~ if inputChild.hasAttribute('episode') :
    #~ itemOutputNode.appendChild(domOutput.createNode('episode',inputChild.getAttribute('episode')))
    return itemOutputNode


def domTreeview() :
    '''
        Retourne un domDocument pour la vue arbre de MythNetVision
    '''

    catalog = json.load(urlopen(urlApi+'catalogue.json'))

     # dom xml de sortie
    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://backend.userland.com/creativeCommonsRssModule',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                }
        ))

    # node channel
    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    for gnrID, gnrValues in catalog['gnrList'].items() :
        # node directory
        logging.debug('Directory '+gnrValues['name']+' ('+gnrID+')')
        if type(gnrValues['img']) == dict :
            thumbnail = urlImage+gnrValues['img']['vignette']
        else :
            thumbnail = 'w9replay.png'
        directoryOutputNode = channelOutputNode.appendChild(domOutput.createNode(
                tag = 'directory',
                attributes = {
                        'name': gnrValues['name'],
                        'thumbnail': thumbnail
                    }
            ))
        for pgmID, pgmValues in catalog['pgmList'].items() :
            if pgmValues['idGnr'] and int(pgmValues['idGnr']) == int(gnrID) :
                # node subdirectory
                logging.debug('+-Subirectory '+pgmValues['name']+' ('+pgmID+')')
                if type(pgmValues['img']) == dict :
                    thumbnail = urlImage+pgmValues['img']['vignette']
                else :
                    thumbnail = 'w9replay.png'

                subDirectoryOutputNode = domOutput.createNode(
                        tag = 'directory',
                        attributes = {
                                'name': pgmValues['name'],
                                'thumbnail': thumbnail
                            }
                    )
                del(catalog['pgmList'][pgmID])

                has_childs = False
                for clpID, clpValues in catalog['clpList'].items() :
                    if clpValues['idPgm'] and int(clpValues['idPgm']) == int(pgmID) :
                        has_childs = True
                        # node item
                        logging.debug('| +-Video '+clpValues['clpName']+' ('+clpID+')')
                        subDirectoryOutputNode.appendChild(domItemNode(domOutput,clpValues))
                        del(catalog['clpList'][clpID])
                if has_childs :
                    directoryOutputNode.appendChild(subDirectoryOutputNode)



    # node directory non classé
    logging.debug('Directory unclassed')
    directoryOutputNode = channelOutputNode.appendChild(domOutput.createNode(
            tag = 'directory',
            attributes = {
                    'name': 'Non classés',
                    'thumbnail': 'w9replay.png'
                }
        ))
    for pgmID, pgmValues in catalog['pgmList'].items() :
        # node subdirectory
        logging.debug('+-Subirectory '+gnrValues['name']+' ('+pgmID+')')
        if type(pgmValues['img']) == dict :
            thumbnail = urlImage+pgmValues['img']['vignette']
        else :
            thumbnail = 'w9replay.png'

        subDirectoryOutputNode = domOutput.createNode(
                tag = 'directory',
                attributes = {
                        'name': pgmValues['name']+' ('+pgmID+')',
                        'thumbnail': thumbnail
                    }
            )
        del(catalog['pgmList'][pgmID])

        has_childs=False
        for clpID, clpValues in catalog['clpList'].items() :
            if clpValues['idPgm'] and int(clpValues['idPgm']) == int(pgmID) :
                has_childs=True
                # node item
                logging.debug('| +-Video '+clpValues['clpName']+' ('+clpID+')')
                subDirectoryOutputNode.appendChild(domItemNode(domOutput,clpValues))
                del(catalog['clpList'][clpID])
        if has_childs :
            directoryOutputNode.appendChild(subDirectoryOutputNode)

    # node subdirectory unclassed
    logging.debug('+-Subirectory unclassed')
    subDirectoryOutputNode = domOutput.createNode(
            tag = 'directory',
            attributes = {
                    'name': 'Non classés',
                    'thumbnail': 'w9replay.png'
                }
        )

    has_childs=False
    for clpID, clpValues in catalog['clpList'].items() :
        has_childs=True
        # node item
        logging.debug('| +-Video '+clpValue['clpName'])
        subDirectoryOutputNode.appendChild(domItemNode(domOutput,clpValues))
    if has_childs :
        directoryOutputNode.appendChild(subDirectoryOutputNode)

    return domOutput


def domSearch (text,page) :
    '''
        Retourne un domDocument pour la recherche de MythNetVision
    '''

    catalog = json.load(urlopen(urlApi+'catalogue.json'))

    text = text.lower() # recherche insensible à la case

    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://cnettv.com/mrss/',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                    'xmlns:mythtv':'http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format'
                }))

    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    for clpValues in catalog['clpList'].values() :
        if clpValues['programName'].lower().find(text) != -1  \
                or clpValues['clpName'].lower().find(text) != -1 \
                or clpValues['desc'].lower().find(text) != -1 :
            # node item
            logging.debug('find: '+clpValues['programName']+' - '+clpValues['clpName'])
            channelOutputNode.appendChild(domItemNode(domOutput,clpValues,True))

    return domOutput


if __name__ == "__main__" :

    #Charge le config utilisateur si elle existe
    config = RawConfigParser()
    if os.path.exists(userConfigFile) :
        config.read(userConfigFile)
    elif os.path.exists(configFile) :
        config.read(configFile)
    if config.has_section('external_commands') :
        if config.has_option('external_commands','player') :
            player = config.get('external_commands','player')
        if config.has_option('external_commands','playerargs') :
            playerargs = config.get('external_commands','playerargs')
        if config.has_option('external_commands','download') :
            player = config.get('external_commands','download')
        if config.has_option('external_commands','downloadargs') :
            playerargs = config.get('external_commands','downloadargs')

    opts,args = getopt(sys.argv[1:], "hvS:Tp:",["help","version","search=","treeview","page="])
    if len(opts) == 0 :
        sys.stdout.write(__usage__)
        sys.exit(0)

    search = False
    page = 1
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(0)
        elif opt in ("-v", "--version"):
            sys.stdout.write(domVersion().toxml())
            sys.exit(0)
        elif opt in ("-S", "--search"):
            search = arg
        elif opt in ("-p", "--page"):
            page = int(arg)
        elif opt in ("-T", "--treeview"):
            sys.stdout.write(domTreeview().toxml())
            sys.exit(0)

    if search :
        sys.stdout.write(domSearch(search,page).toxml())
        sys.exit(0)
